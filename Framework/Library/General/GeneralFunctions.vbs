'********** Open Web Browser IE, Chrome, FireFox **********
Public Sub FW_OpenWebBrowser(strTransName, strUrl, strBrowser)
	Call FW_TransactionStart(strTransName)
	If UCase(strBrowser) = "IE" Then
   		SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe", strUrl, "", ""
	ElseIf UCase(strBrowser) = "CHROME" Then
		SystemUtil.Run "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", strUrl, "", "", 3
	ElseIf UCase(strBrowser) = "FIREFOX" Then
		SystemUtil.Run "C:\Program Files (x86)\Mozilla Firefox\firefox.exe", strUrl, "", ""
	End If
	Wait 5
	Call FW_CaptureScreenShot("", strTransName)
	Call SaveStatus("Pass", myFile, "")
	Call FW_TransactionEnd(strTransName)
End Sub
'**********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserChrome(transName)
	On Error Resume Next    
	Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'chrome.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	Call SaveStatus("Pass", "", "")
	Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserChromeWhenError()
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'chrome.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call SaveStatus("Pass", "", "")
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserFireFox(transName)
	On Error Resume Next    
	Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'firefox.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserFireFoxWhenError()
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'firefox.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserIE(transName)
	On Error Resume Next    
	Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'iexplore.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserIEWhenError()
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'iexplore.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Current **********
Public Sub FW_CloseCurrentBrowser(transName)
	On error resume next    
	Call FW_TransactionStart(transName)
	If Browser(obj_Browser).Exist(WAIT_SHORT) Then
		Browser(obj_Browser).Close
	Else
		FW_PrintInfo "micFail", obj_Browser, obj_Browser & " Browser Cannot Close Error Description : " & Err.Description
		Err.Clear                                                                                               
	End If
	Call FW_TransactionEnd(transName)
End Sub
'***********************************

'********* Compare Text Result *********
Public Function CompareTextResult(strExpected, strActual, strStepName, strDetail)
	On Error Resume Next    
	If strExpected = strActual Then
		Reporter.ReportEvent micPass, strStepName, strDetail
		Print "- Compare Text Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Compare Text Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine
		strResultStatus = "Pass"
		FW_CaptureScreenShot("")
	Else
		Reporter.ReportEvent micFail, strStepName, strDetail
		Print "- Compare Text Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Compare Text Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetails & vbNewLine	
		strResultStatus = "Fail"
		FW_CaptureScreenShot("")
	End If
	 
End Function
'***************************************

'********* Report Result *********
Public Function ReportResult(strStatus,strStepName,strDetail)
	If LCase(strStatus) = "micpass" or LCase(strStatus) = "pass" or LCase(strStatus) = "exist" or LCase(strStatus) = "true" or LCase(strStatus) = "matching" Then
		Reporter.ReportEvent micPass, strStepName, strDetail
		print "- Report Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Report Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine
		strResultStatus="Pass"	
		FW_CaptureScreenShot("")
	Else
		Reporter.ReportEvent micFail, strStepName, strDetail
		print "- Report Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Report Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine	
		strResultStatus = "Fail"	
		FW_CaptureScreenShot("")	'Call Function SnapShot Imag png
	End If
End Function
'*********************************

'********* PrintInfo *********
Public Function FW_PrintInfo(intStatus, strSource, strDetail)
	If intStatus = "micPass" Then
		Reporter.ReportEvent micPass, strSource, strDetail
	Else
		Reporter.ReportEvent micFail, strSource, strDetail	                              			
	End If
	Print intStatus & " " & strSource & " " & strDetail
End Function
'*****************************

'********* Create Folder *********
Public Function CreateFolder(strFolderName)
	strFolderPath = strFolderName		'"C:\SampleFolder"
	Set obj = Createobject("Scripting.FileSystemObject")
	If obj.FolderExists(strFolderPath) = False Then
		obj.CreateFolder strFolderPath
		'obj.CreateFolder StrTemp
	End If
	Set obj = Nothing
End Function
'*********************************

'********* Create Sub Folder *********
Public Function CreateSubFolder(strSubFolderName)
	strSubFolderPath = strSubFolderName		'"C:\SampleFolder"
	Set obj = Createobject("Scripting.FileSystemObject")
	If obj.FolderExists(strSubFolderPath) = False Then
	 obj.CreateFolder strSubFolderPath
	End If
	Set obj = Nothing
End Function
'*************************************

'********* Show File List in Folder *********
Function ShowFileList(folderspec)
	Dim fso, f, f1, fc, s
   	Set fso = CreateObject("Scripting.FileSystemObject")
   	Set f = fso.GetFolder(folderspec)
   	Set fc = f.Files
   	For Each f1 in fc
		s = s & f1.name 
		s = s &   "<BR>"
   	Next
   	ShowFileList = s
End Function
'********************************************

'********* Report File Status *********
Function ReportFileStatus(filespec)
	Dim fso, msg
   	Set fso = CreateObject("Scripting.FileSystemObject")
   	If (fso.FileExists(filespec)) Then
    	msg = filespec & " exists."
   	Else
   		msg = filespec & " doesn't exist."
  	End If
   	ReportFileStatus = msg
End Function
'**************************************

'********* Show Folder Size *********
Function ShowFolderSize(filespec)
   Dim fso, f, s
   Set fso = CreateObject("Scripting.FileSystemObject")
   Set f = fso.GetFolder(filespec)
   's = UCase(f.Name) & " uses " & f.size & " bytes."
   'ShowFolderSize = s
   ShowFolderSize = f.size
End Function
'************************************

'********* Delete File *********
Function DeleteFile(sourceFile)
	Set fso = CreateObject("Scripting.FileSystemObject")
	Print "DeleteFile : " & sourceFile
	'SourceFile="D:\Automate\PromptPay\Result\20170908_101715\20170908_101715_Result.xlsx"
   	If (fso.FileExists(sourceFile)) Then
      	'File to be  deleted.  Sourcefile="C:\copy.txt"  'Delete the file
      	fso.DeleteFile sourceFile
      	'print "Source File Delete : "&SourceFile
   	Else
      	Print "Source File doesn't exist : " & sourceFile
   	End If
	Set fso = Nothing
End Function
'*******************************

'********* Delete Folder *********
Function DeleteFolder(strFolder)
	Dim objFS
	'Create a file system object
	Set objFS = CreateObject("Scripting.FileSystemObject")
	'Check that the source folder exists
	If Not objFS.FolderExists(strFolder) Then
		'fail if the source does not exist
	    Print "Unable to Delete Folder '" & strFolder & "', It Does Not Exist"
	Else
	    'Delete the folder
	    objFS.DeleteFolder strFolder
	End If
	' destroy the object
	Set objFS = Nothing
End Function
'*********************************

'********* Rename Folder *********
Function RenameFolder(strSourceFolder, strDestinationFolder)
	Dim objFS
	'Create a file system object
	Set objFS = CreateObject("Scripting.FileSystemObject")
	objFS.MoveFolder strSourceFolder, strDestinationFolder
	Print "Rename Result Folder " & strSourceFolder & " To " & strDestinationFolder
	Set objFS = Nothing
End Function
'*********************************

'********* Get Current Date Time Format [YYYYMMDD_HHMMSS] *********
Public Function GetCurrentDateTimeFormat
	GetCurrentDateTimeFormat = 0  
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now) 
	End If
	
	If Len(Day(Now)) = 1 Then 
		currentDay = 0 & Day(Now) 
	Else 
		currentDay = Day(Now) 
	End If
	
	If Len(Hour(Now)) = 1 Then 
		currentHour = 0 & Hour(Now) 
	Else 
		currentHour = Hour(Now) 
	End If
	
	If Len(Minute(Now)) = 1 Then    
		currentMinute = 0 & Minute(Now)    
	Else    
		currentMinute = Minute(Now) 
	End If
	
	If Len(Second(Now)) = 1 Then    
		currentSecond = 0 & Second(Now)    
	Else    
		currentSecond = Second(Now) 
	End If
	
	GetCurrentDateTimeFormat = Year(Now) & currentMonth & currentDay & "_" & currentHour & currentMinute & currentSecond  

End Function
'******************************************************************

'********* Capture Screen Shot *********
Public Function FW_CaptureScreenShot(strFileName)
	If LCase(CAPTURE_SCREEN_SHOT_STATUS) = "true" or LCase(CAPTURE_SCREEN_SHOT_STATUS) = "capture" Then
		If strFileName = "" or LCase(strFileName) = "default" Then		'Set "" Auto Define Folder and filename SnapShots
			myFile = strResultPathExecution & testCase & "_" & GetCurrentDateTimeFormat & ".png"
			
			'Myfile= strResultPathTestCase&"\"&strResultStatus&"_"&GetFormatedCurrentDate&".png" 
'			iRowCount = Datatable.getSheet("CapScreen").getRowCount
' 			Datatable.getSheet("CapScreen").setCurrentRow(iRowCount+1)
' 			DataTable.Value("No","CapScreen") = Myfile
		Else
			myFile = strFileName
		End If
		'wait 2
	    Desktop.CaptureBitmap myFile, True
	End If
End Function
'***************************************

'********* Capture Screen Shot *********
Public Function FW_CaptureScreenShot(strFileName, transName)
	If LCase(CAPTURE_SCREEN_SHOT_STATUS) = "true" or LCase(CAPTURE_SCREEN_SHOT_STATUS) = "capture" Then
		If strFileName = "" or LCase(strFileName) = "default" Then		'Set "" Auto Define Folder and filename SnapShots
			myFile = strResultPathExecution & testCase & "#" & RUNNING_STEP & "_" & transName & "_" & GetCurrentDateTimeFormat & ".png"
			
			'Myfile= strResultPathTestCase&"\"&strResultStatus&"_"&GetFormatedCurrentDate&".png" 
'			iRowCount = Datatable.getSheet("CapScreen").getRowCount
' 			Datatable.getSheet("CapScreen").setCurrentRow(iRowCount+1)
' 			DataTable.Value("No","CapScreen") = Myfile
		Else
			myFile = strFileName
		End If
		'wait 2
	    Desktop.CaptureBitmap myFile, True
	End If
End Function
'***************************************

'********* Create Initial FrameWork *********
Public Function FW_CreateInitialFramework()
	strImportFileTestData = PATH_IMPORT_DATA & "\" & FILE_TEST_DATA
	
	'Start Update Date Version and Execute Version
	executeVersion = 0
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = True	
	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets("SummaryReport")
	dateVersion = objWorkSheet.Cells(9,2).value
	executeVersion = objWorkSheet.Cells(10,2).value
	
	If Len(Day(Now)) = 1 Then  
		currentDay = 0 & Day(Now)   
	Else  
		currentDay = Day(Now) 
	End If
	
	currentMonth = ""
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now)
	End If
		
	currentDate = Year(Now) & "" & currentMonth & "" & currentDay
	Print "DateVersion : " & dateVersion
	Print "ExecuteVersion : " & executeVersion
	Print "CurrentDate : " & currentDate
	
	If Trim(dateVersion) = Trim(currentDate) Then
		objWorkSheet.Cells(10,2).value = executeVersion + 1
		executeVersion = objWorkSheet.Cells(10,2).value
		Print "ExecuteVersion Add : " & executeVersion
	Else
		objWorkSheet.Cells(9,2).value = currentDate
		objWorkSheet.Cells(10,2).value = 1
		dateVersion = objWorkSheet.Cells(9,2).value
		executeVersion = objWorkSheet.Cells(10,2).value
		Print "DateVersion Clear : " & dateVersion
		Print "ExecuteVersion Clear : " & executeVersion
	End If
	
	EXECUTE_VERSION = executeVersion
	
	objWorkBook.Save()
	objWorkBook.Close
    objExcel.DisplayAlerts = False
	objExcel.Quit

	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing

	'Import Test Data
	Datatable.Import strImportFileTestData
	EXECUTE_STATUS = "PASS"
	'TestStep
	Datatable.AddSheet("Log")
	DataTable.GetSheet("Log").AddParameter "Log", ""

	'TestStep
'	Datatable.AddSheet("TestStep")
'	DataTable.GetSheet("TestStep").AddParameter "LogStep", ""
'	DataTable.GetSheet("TestStep").AddParameter "LogStatus", ""
'	DataTable.GetSheet("TestStep").AddParameter "CaptureScreenShot", ""
'	DataTable.GetSheet("TestStep").AddParameter "StartTime", ""
'	DataTable.GetSheet("TestStep").AddParameter "EndTime", ""
	
	'LogTransactionTime
	Datatable.AddSheet("TestStep")
	DataTable.GetSheet("TestStep").AddParameter "TestCaseNo" ,""
	DataTable.GetSheet("TestStep").AddParameter "StepName" ,""
	DataTable.GetSheet("TestStep").AddParameter "Status", ""
	DataTable.GetSheet("TestStep").AddParameter "CaptureScreenShot", ""
	DataTable.GetSheet("TestStep").AddParameter "StartTime" ,""
	DataTable.GetSheet("TestStep").AddParameter "EndTime" ,""
	DataTable.GetSheet("TestStep").AddParameter "ErrorDescription" ,""
'	Datatable.AddSheet("CapScreen")
'	DataTable.GetSheet("CapScreen").AddParameter "No" ,""

	'SummaryReport
    Datatable.AddSheet("SummaryReport")
    Datatable.ImportSheet strImportFileTestData, "SummaryReport", "SummaryReport"
    Datatable.getSheet("SummaryReport").setCurrentRow(1)
    DataTable.Value("Result", "SummaryReport") = "'" & startExecuteDate
    Datatable.getSheet("SummaryReport").setCurrentRow(2)
    Call FW_WriteLog("StartExecuteTime", startExecuteTime)
    DataTable.Value("Result", "SummaryReport") = "'" & startExecuteTime
    
	strGetTime = GetCurrentDateTimeFormat
	Call FW_WriteLog("GetTime", strGetTime)
	strResultPathExecution = PATH_EXPORT_RESULT & strGetTime & "\"
	strResultTmpPathExecution = PATH_EXPORT_RESULT & strGetTime & "\" & "Tmp" & "\"  'subfolder name
	strTmpResultPathExecution = PATH_EXPORT_RESULT & strGetTime & "\" & "TMP"
	strResultFileExecution = strGetTime
	strResultFileName = strResultPathExecution & R_FILENAME_RESULT & strResultFileExecution & "_" & EXECUTE_VERSION
	'FileTMPNameResult = strResultTmpPathExcel & strResultFileExcel
	ResultTmpFileName = strResultTmpPathExecution & strResultFileExecution
	
	Print strResultPathExecution
	Print strResultFileName
	Print strResultFileExecution
	
	Call CreateFolder(strResultPathExecution)
	'Call CreateSubFolder(strTmpResultPathExecution)
	Call FW_WriteLog("********************************************", "")
	Call FW_WriteLog("Start Execution", now())
	Call FW_WriteLog("Result Path Execution", strResultPathExecution)
	Call FW_WriteLog("Result FileName", strResultFileName)
	Call FW_WriteLog("********************************************", "")
End Function
'********************************************

'********* Create Result in Data Table *********
Public Function FW_CreateResult(dtRow)
	If Trim(strResultStatus) = "" or Trim(LCase(strResultStatus)) = "n/a" Then
		strResultStatus = "Pass"
	Else
		strResultStatus = "Fail"
	End If

	DataTable.GetSheet("TestController").SetCurrentRow(dtRow)
	DataTable(RESULT_STATUS, "TestController") = strResultStatus
	DataTable(RESULT_DESCRIPTION, "TestController") = strResultDescription
	Call FW_WriteLog("Result Status", strResultStatus)
	Call FW_WriteLog("Result Description", strResultDescription)
	Call FW_WriteLog("End Execution", Now())
	Call FW_WriteLog("********************************************", "")
	strResultDescription = "'"
	strResultStatus = "N/A"
End Function
'***********************************************

'TMP Excel
Public Function FW_Result_Export_Tmp()
	Dim strRowCount
	Datatable.AddSheet("SummaryReport")
	Datatable.getSheet("SummaryReport").setCurrentRow(3)
    DataTable.Value("Result","SummaryReport") = StoptExecuteTime  
    Call SummaryAllTest
   	TmpExecuteDate = GetFormatedCurrentDate()
	OriginalFileExport = FileTMPNameResult&"TMP_Result"&".xlsx"
	'OriginalFileExport = FileTMPNameResult&"TMP_Result"&".xlsx"
	Datatable.ExportSheet OriginalFileExport,"SummaryReport"
	Datatable.ExportSheet OriginalFileExport,"Global"
	Datatable.ExportSheet OriginalFileExport,"Main"
	Datatable.ExportSheet OriginalFileExport,"TestStep"
	Datatable.ExportSheet OriginalFileExport,"LogTimeTransaction"
	Set fso = CreateObject("Excel.Application")
	Set objExcel = fso   	    
	Set objWorkBook = objExcel.Workbooks.Open (OriginalFileExport)
	Set objWorkSheet = objWorkBook.WorkSheets("Global")
	Set objWorkSheetLog = objWorkBook.WorkSheets("Main")
	'	Extend Width in column
	objWorkSheet.Columns("E:E").ColumnWidth = 100
	objWorkSheetLog.Columns("A:A").ColumnWidth = 100
	'	Set left-align the text.
	objWorkSheet.Columns("E:E").HorizontalAlignment = -4131
	'strColumnCount = 30
	'   Get Row Count Range From Range
    strRowCount = objWorkSheet.UsedRange.Rows.Count 
    For StartRow = 1 To strRowCount
        strTestStatus = Trim(LCase(objWorkSheet.Cells(StartRow, "D").Value))
        If StartRow = 1 Then
        	strColorIndexValue = 17 'Color Green Header
        Else 
	        Select Case strTestStatus
	            Case "pass"
	                strColorIndexValue = 43  'Color Green
	            Case "fail"
	                strColorIndexValue = 3  'Color Red
	            Case "no run"
	                strColorIndexValue = 6 'Color Yellow
	            Case Else
	                strColorIndexValue = 2  'Color White
	               	'Print "Test Status Not Match Row " & StartRow & " at : " & OriginalFileExport
	        End Select
        End If
        
        strRange = ""
        If StartRow = 1 Then
        	strRange = "A" & StartRow & ":E" & StartRow
        Else
            strRange = "D" & StartRow & ":D" & StartRow
        End If
        'Change Cell Color follow Test Status 
        objWorkSheet.Range(strRange).Interior.ColorIndex = strColorIndexValue
        
'        For StartColumn = 6 To strColumnCount
'        	strData = "F" & strColumnCount & ":V" & strColumnCount
'		    objWorkSheet.Range(strData).EntireColumn.Delete
'        Next
		
    Next
	
	objWorkbook.SaveAs(FileTMPNameResult&"_"&TmpExecuteDate&"_TMP.xlsx")
'    objworkbook.Saved=True
	'Close workbook
	objworkbook.Close		'commented out to leave workbook open for review
    objExcel.DisplayAlerts = False
	objExcel.Quit    

	Set objWorkSheet = Nothing
	Set objWorkbook = Nothing	
	Set objExcel = nothing
	
	'	Delete Original File Export from DataTable
	Call FileDelete(OriginalFileExport)
End Function

'********* Export Result and Custom Result *********
Public Function FW_ExportResult()
	Dim strRowCount
	'Datatable.AddSheet("Summary")
	Datatable.getSheet("SummaryReport").setCurrentRow(3)
    DataTable.Value("Result", "SummaryReport") = "'" & stopExecuteTime  
    Call SummaryAllTest()
    
	originalFileExport = strResultFileName & ".xlsx"
	'Datatable.ExportSheet originalFileExport, "Global"
	Datatable.ExportSheet originalFileExport, "TestController"
	Datatable.ExportSheet originalFileExport, "SummaryReport"
    Datatable.ExportSheet originalFileExport, "TestStep"
	'Datatable.ExportSheet originalFileExport, "LogTransactionTime"
	'Datatable.ExportSheet originalFileExport, "Log"
	
	Set fso = CreateObject("Excel.Application")
	Set objExcel = fso   	    
	Set objWorkBook = objExcel.Workbooks.Open(originalFileExport)
	'Set objWorkSheet = objWorkBook.WorkSheets("Global")
	
	Set objWorkSheet = objWorkBook.WorkSheets("TestController")
	'Set objWorkSheetLogTransactionTime = objWorkBook.WorkSheets("LogTransactionTime")
	Set objWorkSheetSummaryReport = objWorkBook.WorkSheets("SummaryReport")
	Set objWorkSheetTestStep = objWorkBook.WorkSheets("TestStep")
	'Set objWorkSheetLog = objWorkBook.WorkSheets("Log")
	
	'Extend Width in column
	objWorkSheet.Columns("A:A").ColumnWidth = 15
	objWorkSheet.Columns("B:B").ColumnWidth = 50
	objWorkSheet.Columns("C:C").ColumnWidth = 10
	objWorkSheet.Columns("D:D").ColumnWidth = 10
	objWorkSheet.Columns("E:E").ColumnWidth = 100
	'Set left-align the text.
	objWorkSheet.Columns("E:E").HorizontalAlignment = -4131
	objWorkSheet.Range("A:E").Font.Name = "Arial"
	
'	'TestStep
'	'Extend Width in column
'	objWorkSheetTestStep.Columns("A:A").ColumnWidth = 40
'	objWorkSheetTestStep.Columns("B:B").ColumnWidth = 15
'	objWorkSheetTestStep.Columns("C:C").ColumnWidth = 80
'	objWorkSheetTestStep.Columns("D:D").ColumnWidth = 15
'	objWorkSheetTestStep.Columns("E:E").ColumnWidth = 15
'	'Change Cell Color Log
'	objWorkSheetTestStep.Range("A:E").Font.Name = "Arial"
'    objWorkSheetTestStep.Range("A1:E1").Interior.ColorIndex = 27
'    objWorkSheetTestStep.Range("A1:E1").Font.ColorIndex = 1
'    'Change Cell Font Bold Log
'    objWorkSheetTestStep.Range("A1:E1").Font.Bold = True
	
	'TestStep
	'Extend Width in column
	objWorkSheetTestStep.Columns("A:A").ColumnWidth = 15
	objWorkSheetTestStep.Columns("B:B").ColumnWidth = 50
	objWorkSheetTestStep.Columns("C:C").ColumnWidth = 15
	objWorkSheetTestStep.Columns("D:D").ColumnWidth = 50
	objWorkSheetTestStep.Columns("E:F").ColumnWidth = 15
	objWorkSheetTestStep.Columns("G:G").ColumnWidth = 50
	'Change Cell Color Log
	objWorkSheetTestStep.Range("A:G").Font.Name = "Arial"
    objWorkSheetTestStep.Range("A1:G1").Interior.ColorIndex = 27
    objWorkSheetTestStep.Range("A1:G1").Font.ColorIndex = 1
    'Change Cell Font Bold Log
    objWorkSheetTestStep.Range("A1:G1").Font.Bold = True
	
	'SummaryReport
	'Extend Width in column
	objWorkSheetSummaryReport.Columns("A:A").ColumnWidth = 30
	objWorkSheetSummaryReport.Columns("B:B").ColumnWidth = 10
	'Change Cell Color Log
	objWorkSheetSummaryReport.Range("A:B").Font.Name = "Arial"
    objWorkSheetSummaryReport.Range("A1:B1").Interior.ColorIndex = 27
    objWorkSheetSummaryReport.Range("A1:B1").Font.ColorIndex = 1
    'Change Cell Font Bold Log
    objWorkSheetSummaryReport.Range("A1:B1").Font.Bold = True
	
	'Log
	'Extend Width in column
'	objWorkSheetLog.Columns("A:A").ColumnWidth = 100
'	objWorkSheetLog.Range("A:A").Font.Name = "Arial"
'	'Change Cell Color Log
'    objWorkSheetLog.Range("A1:A1").Interior.ColorIndex = 27
'    objWorkSheetLog.Range("A1:A1").Font.ColorIndex = 1
'    'Change Cell Font Bold Log
'    objWorkSheetLog.Range("A1:A1").Font.Bold = True
    
	'strColumnCount = 30
	'Get Row Count Range From Range
    strRowCount = objWorkSheet.UsedRange.Rows.Count 
    For startRow = 1 To strRowCount
        strTestStatus = Trim(LCase(objWorkSheet.Cells(startRow, "D").Value))
        If startRow = 1 Then
        	strColorIndexValue = 27 'Color Light Yellow Header
        Else 
	        Select Case strTestStatus
	            Case "pass"
	                strColorIndexValue = 4  'Color Light Green
	            Case "fail"
	                strColorIndexValue = 3  'Color Light Red
	            Case "no run"
	                strColorIndexValue = 48 'Color Yellow
	            Case Else
	                strColorIndexValue = 2  'Color White
	               	'Print "Test Status Not Match Row " & StartRow & " at : " & OriginalFileExport
	        End Select
        End If
        
        strRange = ""
        If startRow = 1 Then
        	strRange = "A" & startRow & ":E" & startRow
        Else
            strRange = "D" & startRow & ":D" & startRow
        End If
        'Change Cell Color follow Test Status 
        objWorkSheet.Range(strRange).Interior.ColorIndex = strColorIndexValue
        objWorkSheet.Range(strRange).Font.ColorIndex = 1
        
        'Change Cell Font Bold 
        objWorkSheet.Range(strRange).Font.Bold = True
        
'        For StartColumn = 6 To strColumnCount
'        	strData = "F" & strColumnCount & ":V" & strColumnCount
'		    objWorkSheet.Range(strData).EntireColumn.Delete
'        Next
		
    Next
	
	objWorkBook.SaveAs(strResultFileName & "_" & EXECUTE_STATUS & ".xlsx")
	'objworkbook.Saved=True
	'Close Workbook
	objWorkBook.Close		'Commented out to leave workbook open for review
    objExcel.DisplayAlerts = False
	objExcel.Quit    

	Set objWorkSheetSummaryReport = Nothing
	'Set objWorkSheetLogTransactionTime = Nothing
    Set objWorkSheetTestStep = Nothing
	'Set objWorkSheetLog = Nothing
	Set objWorkSheet = Nothing
	Set objWorkbook = Nothing	
	Set objExcel = Nothing
	
	'Delete Original File Export from DataTable
	Call DeleteFile(originalFileExport)
	
	'Rename Result Folder
	sourceFolder = PATH_EXPORT_RESULT & strResultFileExecution
	destinationFolder = PATH_EXPORT_RESULT & R_FILENAME_RESULT & strResultFileExecution & "_" & EXECUTE_VERSION & "_" & EXECUTE_STATUS 
	Print "SourceFolder --> " & sourceFolder
	Print "DestinationFolder --> " & destinationFolder 
	Call RenameFolder(sourceFolder, destinationFolder)
End Function
'***************************************************

'********** Exit Iteration **********
Public Function FW_ExitIteration(strDescription)
	strResultStatus = "Fail"
	strResultDescription = strResultDescription
	ExitActionIteration
End Function
'************************************ 

'********** Write Log **********
Public Function FW_WriteLog(ByVal sAction, ByVal sValue)
	If sValue = "" Then
		Print sAction
		runLog = runLog & sAction
	Else
		sAction = sAction & " = " & sValue
		Print sAction
		runLog = runLog & sAction
	End If
	runLog = runLog & chr(10)
	iRowCount = Datatable.getSheet("Log").getRowCount
 	Datatable.getSheet("Log").setCurrentRow(iRowCount+1)
	DataTable("Log", "Log") = runLog
End Function
'******************************* 

'********** Start Execute **********
Public Function FW_StartExecute()
	currentDay = ""
	If Len(Day(Now)) = 1 Then  
		currentDay = 0 & Day(Now)   
	Else  
		currentDay = Day(Now) 
	End If
	
	currentMonth = ""
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now) 
	End If
	
	currentHour = ""
	If Len(Hour(Now)) = 1 Then  
		currentHour = 0 & Hour(Now)   
	Else  
		currentHour = Hour(Now) 
	End If

	currentMinute = ""
	If Len(Minute(Now)) = 1 Then  
		currentMinute = 0 & Minute(Now)   
	Else  
		currentMinute = Minute(Now) 
	End If
	
	currentSecond = ""
	If Len(Second(Now)) = 1 Then  
		currentSecond = 0 & Second(Now)   
	Else  
		currentSecond = Second(Now) 
	End If
	
	startExecuteDate = currentDay & "/" & currentMonth & "/" & Year(Now)
	startExecuteTime = currentHour & ":" & currentMinute & ":" & currentSecond
End Function
'***********************************

'********** Stop Execute **********
Public Function FW_StopExecute()
	currentHour = ""
	If Len(Hour(Now)) = 1 Then  
		currentHour = 0 & Hour(Now)   
	Else  
		currentHour = Hour(Now) 
	End If

	currentMinute = ""
	If Len(Minute(Now)) = 1 Then  
		currentMinute = 0 & Minute(Now)   
	Else  
		currentMinute = Minute(Now) 
	End If
	
	currentSecond = ""
	If Len(Second(Now)) = 1 Then  
		currentSecond = 0 & Second(Now)   
	Else  
		currentSecond = Second(Now) 
	End If
	
	stopExecuteTime = currentHour & ":" & currentMinute & ":" & currentSecond
End Function
'**********************************

'********** Start Execute **********
Public Function FW_DurationTime()
	durationTime = ""
	'startExecuteTime
	'stopExecuteTime
	strDuration = 0
	strDuration = DateDiff("s",startExecuteTime, stopExecuteTime)
	Call FW_WriteLog("DurationTime(s) : ", strDuration)
	
	strHour = Round(strDuration / 3600,0)
	If Len(strHour) = 1 Then
		strHour = 0 & strHour 
	Else  
		strHour = strHour 
	End If
	Call FW_WriteLog("Hour : ", strHour)
	
	strMinute = Round(strDuration / 60,0)
	If Len(strMinute) = 1 Then
		strMinute = 0 & strMinute 
	Else  
		strMinute = strMinute 
	End If
	Call FW_WriteLog("Minute : ", strMinute)
	
	strSecond = strDuration mod 60
	If Len(strSecond) = 1 Then
		strSecond = 0 & strSecond 
	Else  
		strSecond = strSecond 
	End If
	Call FW_WriteLog("Second : ", strSecond)
	
	durationTime = strHour & ":" & strMinute & ":" & strSecond
	Call FW_WriteLog("DurationTime(HH:mm:ss) : ", durationTime)
	
End Function

'********** Summary All Test **********
Public Function SummaryAllTest()
	strPass = 0
	strFail = 0
	For dtRow = 1 To DataTable.GetSheet("TestController").GetRowCount Step 1
	DataTable.GetSheet("TestController").SetCurrentRow(dtRow)
	strStatus = DataTable.Value("Status", "TestController")
		If strStatus ="Pass" Then
			strPass = strPass + 1
		ElseIf strStatus = "Fail" Then
			strFail = strFail + 1
		End If
	Next
	
	Call FW_DurationTime()
	
	Datatable.getSheet("SummaryReport").setCurrentRow(4)
	DataTable.Value("Result", "SummaryReport") =  durationTime
	
	Datatable.getSheet("SummaryReport").setCurrentRow(5)
	DataTable.Value("Result", "SummaryReport") =  strPass + strFail
	Datatable.getSheet("SummaryReport").setCurrentRow(6)
	DataTable.Value("Result", "SummaryReport") =  strPass
	
	strColorIndexValue = 43 
	Datatable.getSheet("SummaryReport").setCurrentRow(7)
    DataTable.Value("Result", "SummaryReport") = strFail
    
End Function
'**************************************

'********** Scroll Web Page **********
Public Function ScrollWebPage()
	Set objShell=CreateObject("WScript.Shell")
'	objShell.SendKeys "{PGUP}"
'	objShell.SendKeys "{PGDN}"
'	objShell.SendKeys "{END}"
End Function
'*************************************

'********** SendKey **********
Public Function Sendkey(strkey)
	Dim objShell
	Set objShell=CreateObject("WScript.Shell")
	objShell.SendKeys strkey
	Set objShell = Nothing
'	objShell.SendKeys "{PGUP}"
'	objShell.SendKeys "{PGDN}"
'	objShell.SendKeys "{END}"
End Function
'*****************************

'********** Compore Number Result **********
Public Function CompareNumberResult(strExpected, strActual, strStepName, strDetail)
	On Error Resume Next    
	If CDBL(strExpected) = CDBL(strActual) Then
		Reporter.ReportEvent micPass, strStepName, strDetail
		print "- Compore Number Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Compore Number Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine
		strResultStatus="Pass"
		FW_CaptureScreenShot("")
	Else
		Reporter.ReportEvent micFail, strStepName, strDetail
		print "- Compore Number Result Fail | StepName : " & strStepName & " | Step Detail : " & strDetail	
		strResultDescription = strResultDescription & "- Compore Number Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine	
		strResultStatus="Fail"
		FW_CaptureScreenShot("")
	End If
'*******************************************

End Function

'********** Export Report to Doc **********
Public Function FW_ExportReportToDoc()
	Dim objWord
	Set objWord = CreateObject("Word.Application")
	objWord.Visible = False
	'strLocalCurrTestPath & "\" & TestName &"\DOC\Report.docx"
	originalFileExport = strResultFileName & ".docx"
	Set fso = CreateObject("Scripting.FilesystemObject")
	If fso.FileExists(PATH_EXPORT_RESULT & "Report.docx") Then
		Set objDoc = objWord.Documents.Open (strLocalCurrentTestPath & "\" & batchName & "\DOC\Report.docx")
		objWord.Selection.End
		Key 6,0
		Set objSelection = objWord.Selection
		Set objShape = objDoc.InlineShapes
	Else
		Set objDoc = objWord.Documents.Add()
		Set objSelection = objWord.Selection
		Set objShape = objDoc.InlineShapes
	End If
	
	intRow = DataTable.GetSheet("TestStep").GetRowCount
	
	For intRowIndex = 1 to intRow

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		strTestStep = Trim(DataTable.Value("LogStep","TestStep"))

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		strStatus = Trim(DataTable.Value("LogStatus","TestStep"))

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		'Myfile = Trim(DataTable.Value("CapScreen","TestStep"))
		myFile = DataTable.Value("CaptureScreenShot","TestStep")

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		strStartTime = DataTable.Value("StartTime", "TestStep")
 		strEndTime = DataTable.Value("EndTime", "TestStep")

		Set fso = Nothing
		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Test Script ID : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strTestStep)
		objSelection.TypeParagraph()
	
		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Test Step : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strStatus)
		objSelection.TypeParagraph()

		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Start Time is : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strStartTime)
		objSelection.TypeParagraph()

		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "End Time is : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strEndTime)
		objSelection.TypeParagraph()

		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Test Step : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strStatus)
		objSelection.TypeParagraph()
		objSelection.InlineShapes.AddPicture(myFile)
		objSelection.TypeParagraph()

'		intRow = DataTable.GetSheet("CapScreen").GetRowCount
'		
'		For Iterator = 1 To intRow
'			DataTable.GetSheet("CapScreen").SetCurrentRow(intRowIndex)
'			Myfile = Trim(DataTable.Value("No","CapScreen"))
'			objSelection.InlineShapes.AddPicture(Myfile)
'		Next
	Next
	
	objDoc.SaveAs2(strResultFileName & "Report.docx")
	objDoc.Close
	objWord.Quit
	
End Function
'******************************************

'********** Transaction Start **********
Public Sub FW_TransactionStart(strTransaction)
	Call StartTime(strStartTime)
	Services.StartTransaction strTransaction
	Call FW_WriteLog("FW_TransactionStart StartTime : " & strStartTime & ", StartTransaction : " & strTransaction, "")
	Call SaveLogTransactionTimeStart(strStartTime, strTransaction)
	Wait 0.5
End Sub
'***************************************

'********** Transaction End **********
Public Sub FW_TransactionEnd(strTransaction)
	Call EndTime(strEndTime)
	Services.EndTransaction strTransaction
	Call FW_WriteLog("FW_TransactionEnd EndTime : " & strEndTime & ", EndTransaction : " & strTransaction, "")
	Call SaveLogTransactionTimeEnd(strEndTime)
	Wait 0.5
	If strResultStatus = "Fail" Then
		Call FW_CloseWebBrowserChromeWhenError()
		Call FW_CloseWebBrowserFireFoxWhenError()
		Call FW_CloseWebBrowserIEWhenError()
		EXECUTE_STATUS = "FAIL"
		Call FW_WriteLog("ExitAction", "")
		ExitAction
	End If
End Sub
'*************************************

Public sub MaximizePage(actionName,Strvalue)
	On error resume next 
	   If JavaWindow(Strvalue).Exist(30) Then
	     JavaWindow(Strvalue).Maximize
	   Else
	     call Rerun(actionName)
	   End If
		
	
End sub



Public Sub MRAppClickMenuMx3(actionName,obj_WpfWindow,strWebEdit)
	On error resume next  
	Call StartTime(StrStartTime)
	'WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click

	If strWebEdit <> "" Then
		With JavaWindow(obj_WpfWindow)			
   			If .JavaStaticText(strWebEdit).Exist(60) Then
				JavaWindow(obj_WpfWindow).JavaStaticText(strWebEdit).Click 8,18,"LEFT"

        			Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", "WebButton Click", strWebEdit & " WebButton not found." 
			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)    
				call Rerun(actionName)				
         	End If         	
		End With			
	End If
End Sub

Public Function FUNC_Close_Current_Browser()
	On error resume next    
	If 	Browser(obj_Browser).Exist(Wait_Short) Then
		Browser(obj_Browser).Close
	Else
		PrintInfo "micFail", obj_Browser, obj_Browser & " Browser Cannot Close Error Description: " & Err.Description
		Err.clear                                                                                               
	End If
End Function

Public sub Mu_selecttrad(obj_JavaWindow,strWebEdit,strjava)
JavaWindow(obj_JavaWindow).JavaInternalFrame(strWebEdit).JavaTree(strjava).Select "#0;#0;#0"
End sub

Public Sub MRAppSelectmenuOpen(obj_WpfWindow,strWebEdit,strvalue)
	On error resume next  
	Call StartTime(StrStartTime)
	'WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click



	If strWebEdit <> "" Then
		With JavaWindow(obj_WpfWindow)			
   			If .JavaMenu(strWebEdit).Exist(60) Then
				JavaWindow(obj_WpfWindow).JavaMenu(strWebEdit).JavaMenu(strvalue).Select

        			Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", "WebButton Click", strWebEdit & " WebButton not found." 
			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)    			   
         	End If         	
		End With			
	End If
End Sub




Public Sub MRCheckButtonJavaWindow(actionName,obj_JavaWindow,strWebEdit)



	On error resume next  
	Call StartTime(StrStartTime)
	If obj_JavaWindow <> "" Then	
	'JavaWindow("MX.3 - MUREX_SIT_C2").JavaButton("Start_Login").Click
	'WpfWindow("Micro Focus MyFlight Sample").WpfObject("John Smith").GetROProperty("text")



		With JavaWindow(obj_JavaWindow)			
   			If .JavaButton(strWebEdit).Exist(60) Then		   
  				Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", strWebEdit, strWebEdit & "WebEdit not found."   
   			    StrStatus = strWebEdit & "Step is False"
   			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation) 	
				call Rerun(actionName)
         	End If         	
		End With			
	End If
End Sub 

Public Sub MUloopchekPackedNumber(PackedNumber)

If  JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Trade Validation").Exist(60) Then
	a = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Trade Validation").GetROProperty("rows")
'MsgBox a
	b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Trade Validation").GetROProperty("cols")
'MsgBox a
		
		For i = 0 to a - 1
		C = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Trade Validation").GetCellData (i,18)
		C = cdbl (C)
		'MsgBox C
		PackedNumber = cdbl (PackedNumber)
			If C = PackedNumber Then
			k = "#"
			g = k & i
			JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("JTable").SetCellData g ,"Selection","1"
			Exit for
			else
			
			End If
		
		Next 
		
End If

End Sub 



Public Sub MuJavaCheckBox(obj_WpfWindow,obj_frame,strWebEdit,strvalue)


JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaCheckBox(strWebEdit).Set strvalue

End Sub 


Public Sub MuselectValidate(obj_WpfWindow,obj_frame,strWebEdit)

If JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTree(strWebEdit).Exist(60) Then
	JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTree(strWebEdit).Select "#0;#1;#3"
End If

End Sub 


Public Sub MuLoopDupicate(strvalue)



For Iterator = 1 To strvalue
		wait 1
		JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("e-Tradepad").JavaTable("4 legs_3").Click 211,24,"RIGHT"
		wait 1
		JavaWindow("MX.3 - MUREX_SIT_C2_MX5").JavaInternalFrame("e-Tradepad").JavaMenu("Duplicate leg").Select

Next


End Sub 



Public Sub MUloopchekPackedNumberRow(PackedNumber)

	If  JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame_2").JavaTable("Trade Validation").Exist(60) Then
	a = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame_2").JavaTable("Trade Validation").GetROProperty("rows")
'MsgBox a
	b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame_2").JavaTable("Trade Validation").GetROProperty("cols")
'MsgBox a
		
		For i = 0 to 100
		C = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame_2").JavaTable("Trade Validation").GetCellData (i,23)
		'C = cdbl (C)
		'PackedNumber = cdbl (PackedNumber)
			If C = PackedNumber Then
			k = "#"
			g = k & i
			JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame_2").JavaTable("JTable").SetCellData g ,"Selection","1"

			else
			
			End If
		
		Next 
End If
End Sub 




Public Sub MUloopchekPackedNumberValidate(PackedNumber)
If  JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").Exist(60) Then
	a = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("rows")
	b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
		For i = 0 to a
		C = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetCellData (i,8)
			If C = PackedNumber Then
			k = "#"
			g = k & i
			JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Document ID").SetCellData g ,"Selection","1"
			Exit for
			else
			
			End If
		
		Next 
End If 

End Sub 


Public Sub MUloopchekPackedMoniter(PackedNumber)

For j = 1 To 20000
	
	a = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Trade Validation").GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 0 To a - 1
		
		C = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Trade Validation").GetCellData (i,1)
		C = replace(C," ","")
		print C
		print PackedNumber
		PackedNumber = replace(PackedNumber," ","")
		If C = PackedNumber Then
			k = "Pass"
			exit for
		End If
	Next
	If k = "Pass" Then
		Exit for
	End If
Next	


End Sub 

Public Sub MUloopchekPackedMoniterACK(obj_WpfWindow,obj_frame,strWebEdit,PackedNumber)

For j = 1 To 100
	
	a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 1 To a -1
	
		C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,1)
		If C = PackedNumber Then
			k = "Pass"
			exit for
		End If
	Next
	If k = "Pass" Then
		Exit for
	End If
Next	


End Sub 



 



Public Sub MuLoopSettlementFlows(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 1 To a - 1
	
		C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,18)
		C = replace(C," ","")
		PackedNumber = replace(PackedNumber," ","")
		If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
			Exit for
		End If

	Next
End Sub

Public Sub MuLoopTypology(obj_WpfWindow,obj_frame,strWebEdit,PackedNumber)
 
a= JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
b = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("cols")

 
 For i = 1 To a
	
		C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,0)

		If C = PackedNumber Then		
			k = "#"
			g = k & i
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).SelectRow g
			Exit for
		End If

	Next
End Sub
Public Sub MRAppButton_Click_frameCheckBox(actionName,obj_WpfWindow,obj_frame,strWebEdit,strvalue)
	On error resume next  
	Call StartTime(StrStartTime)
	'WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click

	If strWebEdit <> "" Then
		With JavaWindow(obj_WpfWindow)			
   			If .JavaInternalFrame(obj_frame).JavaCheckBox(strWebEdit).Exist(60) Then
   			   .JavaInternalFrame(obj_frame).JavaCheckBox(strWebEdit).highlight
   			   .JavaInternalFrame(obj_frame).JavaCheckBox(strWebEdit).Set strvalue	 		   
		   
        			Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", "WebButton Click", strWebEdit & " WebButton not found." 
			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation) 
				call Rerun(actionName)
         	End If         	
		End With			
	End If
End Sub


Public Function Killservicejava()
	On error resume next    
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = 'java.EXE'")
	For Each objProcess in colProcessList
	objProcess.Terminate()
	Next
End Function



Public Sub MuLoopSettlementFlows2(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a

	For i = 0 To a -1
	
		C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,5)
		PackedNumber = replace(PackedNumber," ","")
		C = replace(C," ","")
		
		
		If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
		End If

	Next
End Sub


Public Function Rerun(actionName,obj_WpfWindow)
	On Error Resume Next
	If JavaWindow(obj_WpfWindow).JavaButton("toprightbox_close").Exist(60) Then
'		RunAction actionName, oneIteration
		JavaWindow(obj_WpfWindow).JavaButton("toprightbox_close").Click
			'MRAppButton_Click2 "MX.3 - MUREX_SIT_C2_MX","toprightbox_close"
		JavaWindow(obj_WpfWindow).JavaDialog("ButtonYes").Click
			'MRAppButton_Click_Dinalog2 "MX.3 - MUREX_SIT_C2_MX","MX.3","ButtonYes"
			RunAction actionName, oneIteration
	else
		RunAction actionName, oneIteration
	End If
	

	

        'function code to complete tasks
			
	'If Err.Number <> 0 Then
		'RunAction actionName, oneIteration
	'End If
End Function



Public Sub MuLoopSettlementFlowsACK(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a

	For i = 0 To PackedNumber
	
		'C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,6)
		'PackedNumber = replace(PackedNumber," ","")
		'C = replace(C," ","")
		'If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
		'End If

	Next
End Sub




Public Sub MuLoopSettlementFlowsTrade(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a

	For i = 0 To PackedNumber
	
		'C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,18)
		'PackedNumber = replace(PackedNumber," ","")
		'C = replace(C," ","")
		
		
		'If C = PackedNumber Then
			k = "#"
			g = k & i
		'	wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
			'Exit for
		'End If

	Next
End Sub


Public Sub MuLoopSettlementFlowsOutright(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 1 To a - 1
	
		C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,6)
		C = replace(C," ","")
		PackedNumber = replace(PackedNumber," ","")
		If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
			'Exit for
		End If

	Next
End Sub


Public Sub MuLoopSettlementFlowPackedNumber(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 1 To PackedNumber
	
		'C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,5)
		'C = replace(C," ","")
		'PackedNumber = replace(PackedNumber," ","")
		'If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
			'Exit for
		'End If

	Next
End Sub



Public Sub MuLoopContrackID(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 0 To PackedNumber
	
		'C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,6)
		'C = replace(C," ","")
		'PackedNumber = replace(PackedNumber," ","")
		'If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
			'Exit for
		'End If

	Next
End Sub


Public Sub MuLoopSettlementFlowsOutright(obj_WpfWindow,obj_frame,strWebEdit,strvalue,PackedNumber)
a = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
	'b = JavaWindow("MX.3 - MUREX_SIT_C2_MX").JavaInternalFrame("MModalInternalFrame").JavaTable("Validate Confirmations").GetROProperty("cols")
'MsgBox a
	For i = 1 To PackedNumber
	
		'C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,6)
		'C = replace(C," ","")
		'PackedNumber = replace(PackedNumber," ","")
		'If C = PackedNumber Then
			k = "#"
			g = k & i
			wait 1
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strvalue).SetCellData g ,"Selection","1"
			'Exit for
		'End If

	Next
End Sub



Public Sub MuLoopTypologyOutright(obj_WpfWindow,obj_frame,strWebEdit,PackedNumber)
 
a= JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("rows")
b = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetROProperty("cols")

 
 For i = 1 To a - 1
	
		C = JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).GetCellData (i,0)
		C = replace(C," ","")
		PackedNumber = replace(PackedNumber," ","")
		print C
		print PackedNumber
		If C = PackedNumber Then		
			k = "#"
			g = k & i
			JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).SelectRow g
			Exit for
		End If

	Next
End Sub



