﻿iRowCount = Datatable.getSheet("TC001 [TC001_CAL_CarPrice]").getRowCount
'คำนวณจากราคารถยนต์
tabNameData = Trim((DataTable("TabName","TC001 [TC001_CAL_CarPrice]")))
'ประเภทรถ
carTypeIdData = Trim((DataTable("CarTypeId","TC001 [TC001_CAL_CarPrice]")))
'ยี่ห้อ
carMakeIdData = Trim((DataTable("CarMakeId","TC001 [TC001_CAL_CarPrice]")))
'รุ่น
carModelIdData = Trim((DataTable("CarModelId","TC001 [TC001_CAL_CarPrice]")))
'ปี
yearData = Trim((DataTable("Year","TC001 [TC001_CAL_CarPrice]")))
'ราคารถ(ไม่รวม VAT)
carPriceNoVATData = Trim((DataTable("CarPriceNoVAT","TC001 [TC001_CAL_CarPrice]")))
'เงินดาวน์
downPaymentData = Trim((DataTable("DownPayment","TC001 [TC001_CAL_CarPrice]")))

'Excepted ถ้าจะซื้อรถราคาเท่านี้ จะต้องผ่อนต่อเดือนเท่าไหร่
expectedMessageData = Trim((DataTable("ExpectedMessage","TC001 [TC001_CAL_CarPrice]")))

'Expected ประเภทรถ
expectedCarTypeIdData = Trim((DataTable("ExpectedCarTypeId","TC001 [TC001_CAL_CarPrice]")))
'Expectedยี่ห้อ
expectedCarMakeIdData = Trim((DataTable("ExpectedCarMakeId","TC001 [TC001_CAL_CarPrice]")))
'Expectedรุ่น
expectedCarModelIdData = Trim((DataTable("ExpectedCarModelId","TC001 [TC001_CAL_CarPrice]")))
'Expected ปี
expectedYearData = Trim((DataTable("ExpectedYear","TC001 [TC001_CAL_CarPrice]")))

'Expected ราคารถ(ไม่รวม VAT)
expectedCarPriceNoVATData = Trim((DataTable("ExpectedCarPriceNoVAT","TC001 [TC001_CAL_CarPrice]")))
'Expected เงินดาวน์
expectedDownPaymentData = Trim((DataTable("ExpectedDownPayment","TC001 [TC001_CAL_CarPrice]")))
expectedDownPaymentBahtData = Trim((DataTable("ExpectedDownPaymentBaht","TC001 [TC001_CAL_CarPrice]")))
expectedHirePurchaseData = Trim((DataTable("ExpectedHirePurchase","TC001 [TC001_CAL_CarPrice]")))
expectedPriceMonthData = Trim((DataTable("ExpectedPriceMonth","TC001 [TC001_CAL_CarPrice]")))

'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("คำนวณจากราคารถยนต์").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebTabStrip("คำนวณจากราคารถยนต์").Select "คำนวณจากราคารถยนต์" @@ script infofile_;_ZIP::ssf1.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebList("CarTypeId").Select "รถเก๋ง"
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebList("CarMakeId").Select "BMW" @@ script infofile_;_ZIP::ssf3.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebList("CarModelId").Select "SERIES 3" @@ script infofile_;_ZIP::ssf4.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebList("Year").Select "2019"
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ราคารถ(ไม่รวมVAT)").Set "1,500,000"
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("เงินดาวน์").Set "15"
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebButton("คำนวนสินเชื่อรถ").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("ถ้าจะซื้อรถราคาเท่านี้").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("อัตราผ่อนชำระ(บาทต่อเดือน)").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ยอดจัดเช่าซื้อ").Set
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("เงินดาวน์บาท").Set


Call FW_OpenWebBrowser("เปิดเวปกรุงศรี มาร์เก็ต รถมือสอง","https://www.krungsrimarket.com/CalculateLoan","CHROME")
'Call FW_ValidateElementMessage("ตรวจสอบแถบคำนวณจากราคารถยนต์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากราคารถยนต์", "คำนวณจากราคารถยนต์")
'Call FW_WebTabStripAndVerifyElementResult("เลือกคำนวณจากราคารถยนต์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากราคารถยนต์", "ถ้าจะซื้อรถราคาเท่านี้", tabNameData, expectedMessageData)
Call FW_WebListAndVerifyResult("เลือกประเภทรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarTypeId", carTypeIdData, expectedCarTypeIdData)
Call FW_WebListAndVerifyResult("เลือกยี่ห้อรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarMakeId", carMakeIdData, expectedCarMakeIdData)
Call FW_WebListAndVerifyResult("เลือกรุ่นรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","CarModelId", carModelIdData, expectedCarModelIdData)
Call FW_WebListAndVerifyResult("เลือกปีรถ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","Year", yearData, expectedYearData)
Call FW_WebEditAndVerifyResult("กรอกราคารถ(ไม่รวมVAT)","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ราคารถ(ไม่รวมVAT)", carPriceNoVATData, expectedCarPriceNoVATData) @@ script infofile_;_ZIP::ssf2.xml_;_
Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์", downPaymentData, expectedDownPaymentData)
Call FW_ValidateEditMessage("ตรวจสอบยอดเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์บาท", expectedDownPaymentBahtData)
Call FW_ValidateEditMessage("ตรวจสอบยอดจัดเช่าซื้อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ยอดจัดเช่าซื้อ", expectedHirePurchaseData)
Call FW_WebButtonAndVerifyElementResult("กดปุ่มคำนวนสินเชื่อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวนสินเชื่อรถ","อัตราผ่อนชำระ(บาทต่อเดือน)",expectedPriceMonthData)
Call FW_CloseWebBrowserChrome("ปิดเวปกรุงศรี มาร์เก็ต รถมือสอง") @@ script infofile_;_ZIP::ssf5.xml_;_

 @@ script infofile_;_ZIP::ssf7.xml_;_
 @@ script infofile_;_ZIP::ssf8.xml_;_




