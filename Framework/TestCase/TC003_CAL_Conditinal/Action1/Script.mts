﻿iRowCount = Datatable.getSheet("TC003 [TC003_CAL_Conditinal]").getRowCount
'คำนวณสินเชื่อแบบระบุเงื่อนไข
tabNameData = Trim((DataTable("TabName","TC003 [TC003_CAL_Conditinal]")))
'ราคารถ(ไม่รวมVAT)
carPriceNoVATData = Trim((DataTable("CarPriceNoVAT","TC003 [TC003_CAL_Conditinal]")))
'เงินดาวน์
downPaymentData = Trim((DataTable("DownPayment","TC003 [TC003_CAL_Conditinal]")))
'ดอกเบี้ย
interestData = Trim((DataTable("Interest","TC003 [TC003_CAL_Conditinal]")))

'ราคารถ(ไม่รวมVAT)
expectedCarPriceNoVATData = Trim((DataTable("ExpectedCarPriceNoVAT","TC003 [TC003_CAL_Conditinal]")))
'เงินดาวน์
expectedDownPaymentData = Trim((DataTable("ExpectedDownPayment","TC003 [TC003_CAL_Conditinal]")))
expectedDownPaymentPriceData = Trim((DataTable("ExpectedDownPaymentPrice","TC003 [TC003_CAL_Conditinal]")))
expectedHirePurchaseData = Trim((DataTable("ExpectedHirePurchase","TC003 [TC003_CAL_Conditinal]")))
'ดอกเบี้ย
expectedInterestData = Trim((DataTable("ExpectedInterest","TC003 [TC003_CAL_Conditinal]")))
expectedPriceMonthData = Trim((DataTable("ExpectedPriceMonth","TC003 [TC003_CAL_Conditinal]")))

'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("คำนวณสินเชื่อแบบระบุเงื่อนไข").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebTabStrip("คำนวณสินเชื่อแบบระบุเงื่อนไข").Select "คำนวณสินเชื่อแบบระบุเงื่อนไข" @@ script infofile_;_ZIP::ssf1.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ราคารถ(ไม่รวมVAT)").Set "2,500,000" @@ script infofile_;_ZIP::ssf2.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("เงินดาวน์").Set "25" @@ script infofile_;_ZIP::ssf3.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ดอกเบี้ย").Set "5" @@ script infofile_;_ZIP::ssf4.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebButton("คำนวนสินเชื่อ").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("เงินดาวน์บาท").Set
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ยอดจัดเช่าซื้อ").Set
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("อัตราผ่อนชำระ(บาทต่อเดือน)").Click

Call FW_OpenWebBrowser("เปิดเวปกรุงศรี มาร์เก็ต รถมือสอง","https://www.krungsrimarket.com/CalculateLoan","CHROME")
'Call FW_ValidateElementMessage("ตรวจสอบแถบคำนวณสินเชื่อแบบระบุเงื่อนไข","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณสินเชื่อแบบระบุเงื่อนไข", "คำนวณสินเชื่อแบบระบุเงื่อนไข")
Call FW_WebTabStrip("เลือกคำนวณสินเชื่อแบบระบุเงื่อนไข","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณสินเชื่อแบบระบุเงื่อนไข", tabNameData)
Call FW_WebEditAndVerifyResult("กรอกราคารถ(ไม่รวมVAT)","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ราคารถ(ไม่รวมVAT)", carPriceNoVATData, expectedCarPriceNoVATData)
Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์", downPaymentData, expectedDownPaymentData)
Call FW_ValidateEditMessage("ตรวจสอบยอดเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์บาท", expectedDownPaymentPriceData)
Call FW_ValidateEditMessage("ตรวจสอบยอดจัดเช่าซื้อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ยอดจัดเช่าซื้อ", expectedHirePurchaseData)
Call FW_WebEditAndVerifyResult("กรอกดอกเบี้ย","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ดอกเบี้ย", interestData, expectedInterestData)
Call FW_WebButtonAndVerifyElementResult("กดปุ่มคำนวนสินเชื่อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวนสินเชื่อ", "อัตราผ่อนชำระ(บาทต่อเดือน)", expectedPriceMonthData)
Call FW_CloseWebBrowserChrome("ปิดเวปกรุงศรี มาร์เก็ต รถมือสอง")
